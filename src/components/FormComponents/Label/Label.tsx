import styled from 'styled-components';

export const Label = styled.label`
  display: block;
  font-size: 16px;
  color: #474747;
  margin-bottom: 10px;
`;
