import React from 'react';
import styled from 'styled-components';
import {
  CustomSelect,
  SelectOption
} from '../FormComponents/CustomSelect/CustomSelect';
import { CURRENCY, CURRENCY_SIGN } from '../../typings/currencies';
import { Nullable } from '../../typings/utils';

const StyledValueContainer = styled.div`
  display: flex;
  justify-content: space-between;
`;

const StyledValue = styled.div`
  font-size: 16px;
  height: 45px;
  line-height: 45px;
  color: rgba(52, 52, 52, 0.87);
`;

type SelectAccount = {
  amount: number;
  currency: CURRENCY;
};

type Props = {
  label: string;
  value: Nullable<string>;
  accounts: SelectAccount[];
  onChange: (value: string) => void;
};

export const AccountsSelect: React.FC<Props> = ({
  label,
  value,
  accounts,
  onChange
}) => {
  const options: SelectOption[] = accounts.map((account: SelectAccount) => ({
    value: account.currency,
    label: `${account.amount} ${CURRENCY_SIGN[account.currency]}`
  }));

  return (
    <CustomSelect
      label={label}
      options={options}
      value={value}
      onChange={onChange}
      valueRenderer={(option: SelectOption) => (
        <StyledValueContainer>
          <StyledValue>{option.label}</StyledValue>
        </StyledValueContainer>
      )}
    />
  );
};
