import React from 'react';
import styled from 'styled-components';

import icon from './exchange.svg';

const StyledIcon = styled.img`
  margin: 30px 15px 0;
`;

export const ExchangeIcon: React.FC = () => <StyledIcon src={icon} />;
