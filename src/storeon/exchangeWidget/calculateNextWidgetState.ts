import { IStoreExchangeWidget, ExchangeWidgetErrors } from './exchangeWidget';
import { Nullable } from '../../typings/utils';

/**
 * Goal of function is encapsule logic of recalculation next state of exchange widget
 *
 * depends on rates and accounts of user:
 * 1) calculate a next toAmount value
 * 2) calculate errors of exchanging
 */

export const calculateNextWidgetState = (
  currentState: IStoreExchangeWidget
): { exchangeWidget: IStoreExchangeWidget } => {
  const nextState = currentState;

  const exchangeResult = calculateExchangeResult(nextState);

  return {
    exchangeWidget: {
      ...nextState,
      ...exchangeResult
    }
  };
};

type ExchangeResult = {
  toAmount: number;
  error: Nullable<string>;
};

export const calculateExchangeResult = (
  widget: IStoreExchangeWidget
): ExchangeResult => {
  const {
    fromAmount,
    toCurrency,
    fromCurrency,
    ratesTable,
    userAccountsTable
  } = widget;

  // const isPreparedForExchange = widget.ratesTable && toCurrency && fromCurrency
  // -> I would prefer to use some kind of a variable for it, but typescript doesn't understand
  //    that I already checked the fact fromCurrency isn't null

  if (
    widget.ratesTable &&
    toCurrency &&
    fromCurrency &&
    widget.ratesTable[toCurrency]
  ) {
    const isEnoughtMoneyOnAccount =
      widget.fromAmount <= userAccountsTable[fromCurrency].amount;

    const exchangeRate = ratesTable[toCurrency];

    return {
      toAmount: fromAmount * exchangeRate,
      error: !isEnoughtMoneyOnAccount
        ? ExchangeWidgetErrors.NOT_ENOUGH_MONEY
        : null
    };
  }

  return {
    toAmount: 0,
    error: null
  };
};
