import styled from 'styled-components';
import MaskedInput from 'react-text-mask';

interface IStyledInput {
  hasError: boolean;
}

export const InputContainer = styled.div``;

export const StyledInput = styled(MaskedInput)`
  background: rgba(255, 255, 255, 0.15);
  border-width: 1px;
  border-style: solid;
  border-color: ${(props: IStyledInput) =>
    props.hasError ? 'rgba(201,17,9,0.55)' : '#dcdce3'};
  border-radius: 4px;
  height: 45px;
  font-size: 16px;
  color: rgba(52, 52, 52, 0.87);
  padding: 0 15px;
  outline: none;

  &:focus {
    border: 1px solid #40a3f5;
  }

  &:disabled {
    background: #f0f0f0;
    border: 1px solid #f0f0f0;
    color: #bababa;
  }
`;
