export enum CURRENCY {
  'EUR' = 'EUR',
  'USD' = 'USD',
  'GBP' = 'GBP'
}

export const CURRENCY_SIGN: Record<string, string> = {
  [CURRENCY.EUR]: '€',
  [CURRENCY.USD]: '$',
  [CURRENCY.GBP]: '£'
};
