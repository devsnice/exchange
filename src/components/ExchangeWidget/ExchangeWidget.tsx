import React, { useMemo, useEffect } from 'react';
import useStoreon from 'storeon/react';

// @ts-ignore TODO: there isn't types for such libraries
import Box from 'react-styled-box';

import { MoneyInput } from '../FormComponents/MoneyInput/MoneyInput';
import { Button } from '../FormComponents/Button/Button';
import { AccountsSelect } from '../AccountsSelect/AccountsSelect';
import { CURRENCY_SIGN } from '../../typings/currencies';
import { ExchangeRate } from '../ExchangeRate/ExchangeRate';
import { ExchangeIcon } from './ExchangeIcon/ExchangeIcon';

import {
  WidgetContainer,
  WidgetHeader,
  WidgetHeading,
  ExchangeHeading
} from './styles';

import { MoneyAccount } from '../../typings/moneyAccount';
import {
  actions as widgetActions,
  IStoreExchangeWidget
} from '../../storeon/exchangeWidget/exchangeWidget';
import { ErrorText } from '../FormComponents/ErrorText/ErrorText';
import { Dispatch } from 'storeon';
import { transformAccountsToTable } from '../../utils/utils';
import { IDataForExchange } from '../../typings/exchangeInfo';

type Props = {
  accounts: MoneyAccount[];
  exchangeMoney: (
    { fromCurrency, toCurrency, amount, rate }: IDataForExchange
  ) => void;
};

const useUpdateUserAccounts = (
  accountsHashTableByCurrency: Record<string, MoneyAccount>,
  dispatch: Dispatch
) => {
  useEffect(
    () => {
      dispatch(widgetActions.updateUserAccounts, {
        userAccountsTable: accountsHashTableByCurrency
      });
    },
    [accountsHashTableByCurrency, dispatch]
  );
};

const useInitWidgetCurrencies = (
  accountsHashTableByCurrency: Record<string, MoneyAccount>,
  dispatch: Dispatch,
  exchangeWidget: IStoreExchangeWidget
) => {
  useEffect(
    () => {
      const userCurrencies = Object.keys(accountsHashTableByCurrency);
      const currenciesNotSet = !(
        exchangeWidget.fromCurrency && exchangeWidget.toCurrency
      );

      if (currenciesNotSet && userCurrencies.length) {
        dispatch(widgetActions.changeFromCurrency, {
          currency: userCurrencies[0],
          dispatch
        });

        dispatch(widgetActions.changeToCurrency, {
          currency: userCurrencies[1]
        });
      }
    },
    [accountsHashTableByCurrency, dispatch]
  );
};

export const ExchangeWidget: React.FC<Props> = ({
  accounts,
  exchangeMoney
}) => {
  const {
    dispatch,
    exchangeWidget
  }: { dispatch: Dispatch; exchangeWidget: IStoreExchangeWidget } = useStoreon(
    'exchangeWidget'
  );

  const accountsHashTableByCurrency: Record<string, MoneyAccount> = useMemo(
    () => transformAccountsToTable(accounts),
    [accounts]
  );

  useUpdateUserAccounts(accountsHashTableByCurrency, dispatch);
  useInitWidgetCurrencies(
    accountsHashTableByCurrency,
    dispatch,
    exchangeWidget
  );

  const exchangeRate =
    exchangeWidget.toCurrency &&
    exchangeWidget.ratesTable[exchangeWidget.toCurrency];

  const formIsValid =
    exchangeWidget.fromCurrency &&
    exchangeWidget.toCurrency &&
    exchangeWidget.fromAmount &&
    exchangeRate;

  return (
    <WidgetContainer>
      <WidgetHeader>
        <WidgetHeading>
          <span role="img">💱</span> Currency exchange
        </WidgetHeading>
      </WidgetHeader>

      <form
        onSubmit={e => {
          e.preventDefault();
          // TODO: typescript dynamic types problem
          if (
            exchangeWidget.fromCurrency &&
            exchangeWidget.toCurrency &&
            exchangeWidget.fromAmount &&
            exchangeRate
          ) {
            exchangeMoney({
              fromCurrency: exchangeWidget.fromCurrency,
              toCurrency: exchangeWidget.toCurrency,
              amount: exchangeWidget.fromAmount,
              rate: exchangeRate
            });
          }
        }}>
        <div>
          <Box margin="0 0 15px 0">
            <AccountsSelect
              label="From"
              accounts={accounts}
              value={exchangeWidget.fromCurrency}
              onChange={(value: string) =>
                dispatch(widgetActions.changeFromCurrency, {
                  currency: value,
                  dispatch
                })
              }
            />
          </Box>

          <AccountsSelect
            label="To"
            accounts={accounts}
            value={exchangeWidget.toCurrency}
            onChange={(value: string) =>
              dispatch(widgetActions.changeToCurrency, {
                currency: value
              })
            }
          />
        </div>

        <div>
          <Box flexDirection="row" alignItems="baseline" margin="30px 0 15px">
            <ExchangeHeading>You change</ExchangeHeading>

            <ExchangeRate
              currencyFrom={exchangeWidget.fromCurrency}
              currencyTo={exchangeWidget.toCurrency}
              rate={exchangeRate}
            />
          </Box>

          <Box justifyContent="space-between">
            <MoneyInput
              placeholder="From your account"
              label={`Total ${exchangeWidget.fromCurrency &&
                CURRENCY_SIGN[exchangeWidget.fromCurrency]}`}
              value={exchangeWidget.fromAmount}
              onChange={(value: string) =>
                dispatch(widgetActions.changeFromAmount, {
                  amount: value
                })
              }
            />
            <ExchangeIcon />
            <MoneyInput
              placeholder="To your account"
              label={`Total ${exchangeWidget.toCurrency &&
                CURRENCY_SIGN[exchangeWidget.toCurrency]}`}
              value={exchangeWidget.toAmount}
              onChange={(value: string) =>
                dispatch(widgetActions.changeToAmount, {
                  amount: value
                })
              }
              readonly
            />
          </Box>
        </div>

        <Box
          margin="30px 0 0"
          justifyContent="space-between"
          alignItems="center">
          <Box>
            <ErrorText error={exchangeWidget.error} />
          </Box>

          <Button disabled={!formIsValid || Boolean(exchangeWidget.error)}>
            Exchange
          </Button>
        </Box>
      </form>
    </WidgetContainer>
  );
};
