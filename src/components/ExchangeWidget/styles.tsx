import styled from 'styled-components';

export const WidgetContainer = styled.section`
  width: 450px;
  max-width: 100%;
`;

export const WidgetHeader = styled.header`
  margin-bottom: 30px;
`;

export const WidgetHeading = styled.h1`
  font-size: 28px;
  font-weight: 700;
`;

export const ExchangeHeading = styled.h3`
  font-size: 20px;
  color: rgba(52, 52, 52, 0.87);
  margin: 0;
  margin-right: 10px;
`;
