import { CURRENCY } from '../typings/currencies';
import { ExchangeInfo } from '../typings/exchangeInfo';

export const REFRESH_RATES_TIMER = 10000;

export type RatesTable = Record<string, ExchangeInfo>;

// This service works only for EUR
const getRatesForEUR = (): Promise<RatesTable> => {
  return fetch(
    `http://data.fixer.io/api/latest?access_key=b77f3fc19be7add24e876733eab53d7c&format=1`
  )
    .then(res => res.json())
    .then(response => response.rates);
};

// Fake rates for USD
const getRatesForUSD = (): Promise<RatesTable> => {
  const USD_TO_EUR_COEF = 0.9;

  return getRatesForEUR().then((rates: RatesTable) => {
    const RatesUSD: RatesTable = {};

    Object.keys(rates).forEach(currency => {
      RatesUSD[currency] = rates[currency] * USD_TO_EUR_COEF;
    });

    return RatesUSD;
  });
};

// Fake rates for GDB
const getRatesForGDB = () => {
  const GDB_TO_EUR_COEF = 2;

  return getRatesForEUR().then((rates: RatesTable) => {
    const RatesUSD: RatesTable = {};

    Object.keys(rates).forEach(currency => {
      RatesUSD[currency] = rates[currency] * GDB_TO_EUR_COEF;
    });

    return RatesUSD;
  });
};

export const getRatesForCurrency = (currency: CURRENCY) => {
  switch (currency) {
    case CURRENCY.EUR:
      return getRatesForEUR();
    case CURRENCY.USD:
      return getRatesForUSD();
    case CURRENCY.GBP:
      return getRatesForGDB();
    default:
      throw new Error(`There isn't api for ${currency}`);
  }
};
