import React from 'react';
import createNumberMask from 'text-mask-addons/dist/createNumberMask';

import { ErrorText } from '../ErrorText/ErrorText';

import { InputContainer, StyledInput } from './style';
import { Label } from '../Label/Label';

type Props = {
  value: number | string;
  onChange: (value: string) => void;
  label: string;
  placeholder?: string;
  error?: string;
  readonly?: boolean;
};

const moneyMask = createNumberMask({
  prefix: '',
  allowLeadingZeroes: false,
  thousandsSeparatorSymbol: '',
  integerLimit: 13,
  allowDecimal: true,
  decimalLimit: 2
});

export const MoneyInput: React.FC<Props> = props => {
  const { label, error, onChange, ...inputProps } = props;

  return (
    <InputContainer>
      <Label>{label}</Label>

      <StyledInput
        {...inputProps}
        hasError={Boolean(error)}
        onChange={e => {
          onChange(e.target.value);
        }}
        mask={moneyMask}
        guide={false}
      />

      <ErrorText error={error} />
    </InputContainer>
  );
};
