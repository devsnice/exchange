import createStore, { Store } from 'storeon';

import {
  exchangeWidgetStore,
  IStoreExchangeWidget
} from './exchangeWidget/exchangeWidget';

import { IStoreUser, userStore } from './user/user';

export interface IStore {
  user: IStoreUser;
  exchangeWidget: IStoreExchangeWidget;
}

export const store = createStore([
  exchangeWidgetStore,
  userStore,
  process.env.NODE_ENV !== 'production' && require('storeon/devtools')
]);
