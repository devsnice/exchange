import { Store } from 'storeon';
import { IStore } from '../../storeon/store';
import { CURRENCY } from '../../typings/currencies';
import { MoneyAccount } from '../../typings/moneyAccount';
import { IDataForExchange } from '../../typings/exchangeInfo';
import { roundMoneyToTwoDigits } from '../../utils/utils';

export interface IStoreUser {
  accounts: MoneyAccount[];
}

const initialState: IStoreUser = {
  accounts: [
    { amount: 1000.5, currency: CURRENCY.EUR },
    { amount: 9000.99, currency: CURRENCY.USD },
    { amount: 44444.99, currency: CURRENCY.GBP }
  ]
};

export const userActions = {
  exchangeMoney: 'exchangeMoney'
};

export const userStore = (store: Store<IStore>) => {
  store.on('@init', () => ({
    user: initialState
  }));

  store.on(
    userActions.exchangeMoney,
    (store, { formData }: { formData: IDataForExchange }) => {
      const exchangeAmount = formData.amount;
      const exchangeResultAmount = exchangeAmount * formData.rate;

      const userAccounts = store.user.accounts;

      const accountsAfterExchange = userAccounts.map(
        (account: MoneyAccount) => {
          if (account.currency === formData.fromCurrency) {
            account.amount = roundMoneyToTwoDigits(
              account.amount - formData.amount
            );
          }
          if (account.currency === formData.toCurrency) {
            account.amount = roundMoneyToTwoDigits(
              account.amount + exchangeResultAmount
            );
          }

          return account;
        }
      );

      return {
        user: {
          accounts: accountsAfterExchange
        }
      };
    }
  );
};
