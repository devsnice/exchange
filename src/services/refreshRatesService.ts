import { RatesTable, REFRESH_RATES_TIMER, getRatesForCurrency } from './api';
import { TimersService } from './timersService';
import { CURRENCY } from '../typings/currencies';

export const refreshRatesService = ({
  id,
  currency,
  updateRates
}: {
  id: string;
  currency: CURRENCY;
  updateRates: (rates: RatesTable) => void;
}) => {
  const lastRefreshTimer = TimersService.getTimer(id);

  clearTimeout(lastRefreshTimer);

  const refreshRates = () => {
    getRatesForCurrency(currency).then(ratesTable => {
      updateRates(ratesTable);

      TimersService.saveTimer(
        id,
        setTimeout(refreshRates, REFRESH_RATES_TIMER)
      );
    });
  };

  refreshRates();
};
