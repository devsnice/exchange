import React from 'react';
import StoreContext from 'storeon/react/context';

import { Normalize } from 'styled-normalize';

import { store } from './storeon/store';
import { ApplicationInterface } from './components/ApplicationInterface/ApplicationInterface';

export const App: React.FC = () => {
  return (
    <StoreContext.Provider value={store}>
      <Normalize />

      <ApplicationInterface />
    </StoreContext.Provider>
  );
};
