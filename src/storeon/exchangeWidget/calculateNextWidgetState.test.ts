import {
  calculateNextWidgetState,
  calculateExchangeResult
} from './calculateNextWidgetState';
import { initialState, ExchangeWidgetErrors } from './exchangeWidget';
import { CURRENCY } from '../../typings/currencies';

describe('ExchangeWidget: calculateNextWidgetState', () => {
  describe('calculateExchangeResult util', () => {
    const widgetState = {
      widgetId: 'test',
      fromCurrency: CURRENCY.USD,
      toCurrency: CURRENCY.EUR,
      fromAmount: 100,
      toAmount: 0,
      userAccountsTable: {
        [CURRENCY.USD]: {
          amount: 1000,
          currency: CURRENCY.USD
        }
      },
      ratesTable: {
        [CURRENCY.EUR]: 2
      },
      error: null
    };

    test('should calculate a correct exchange result, when user has enough balance', () => {
      const exchangeResult = calculateExchangeResult(widgetState);

      expect(exchangeResult).toEqual({
        toAmount: 200,
        error: null
      });
    });

    test('should return an error flag, cause not enough money to exchange', () => {
      const exchangeResult = calculateExchangeResult({
        ...widgetState,
        userAccountsTable: {
          [CURRENCY.USD]: {
            amount: 0,
            currency: CURRENCY.USD
          }
        }
      });

      expect(exchangeResult).toEqual({
        toAmount: 200,
        error: ExchangeWidgetErrors.NOT_ENOUGH_MONEY
      });
    });
  });

  test("should return a correct next state, when there aren't any changes", () => {
    const nextState = calculateNextWidgetState(initialState);

    expect(nextState.exchangeWidget).toEqual(initialState);
  });
});
