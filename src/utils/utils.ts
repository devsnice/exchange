import { MoneyAccount } from '../typings/moneyAccount';

export const transformAccountsToTable = (
  accounts: MoneyAccount[]
): Record<string, MoneyAccount> => {
  const accountsHashTableByCurrency: Record<string, MoneyAccount> = {};

  accounts.forEach(
    account => (accountsHashTableByCurrency[account.currency] = account)
  );

  return accountsHashTableByCurrency;
};

export const roundMoneyToTwoDigits = (number: number) => {
  const utilNumber = 1e2;

  return Math.round(number * utilNumber) / utilNumber;
};

export const roundMoneyToFourDigits = (number: number) => {
  const utilNumber = 1e4;

  return Math.round(number * utilNumber) / utilNumber;
};
