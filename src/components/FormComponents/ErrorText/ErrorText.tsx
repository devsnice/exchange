import React from 'react';
import styled from 'styled-components';
import { Nullable } from '../../../typings/utils';

export const StyledText = styled.div`
  color: #e3150b;
  font-size: 14px;
`;

type Props = {
  error?: Nullable<string>;
};

export const ErrorText: React.FC<Props> = ({ error }) => {
  if (!error) {
    return null;
  }

  return <StyledText>{error}</StyledText>;
};
