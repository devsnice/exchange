import { Store } from 'storeon';
import { IStore } from '../../storeon/store';
import { Nullable } from '../../typings/utils';
import { MoneyAccount } from '../../typings/moneyAccount';
import { calculateNextWidgetState } from './calculateNextWidgetState';
import { CURRENCY } from '../../typings/currencies';
import { ExchangeInfo } from '../../typings/exchangeInfo';
import { refreshRatesService } from '../../services/refreshRatesService';

export const ExchangeWidgetErrors = {
  NOT_ENOUGH_MONEY: `Not enough money to exchange`
};

export interface IStoreExchangeWidget {
  widgetId: string;
  fromCurrency: Nullable<CURRENCY>;
  fromAmount: number;
  toCurrency: Nullable<CURRENCY>;
  toAmount: number;
  userAccountsTable: Record<string, MoneyAccount>;
  ratesTable: Record<string, ExchangeInfo>;
  error: Nullable<string>;
}

export const initialState: IStoreExchangeWidget = {
  widgetId: `exchange-widget-${new Date()}`,
  fromCurrency: null,
  fromAmount: 0,
  toCurrency: null,
  toAmount: 0,
  userAccountsTable: {},
  ratesTable: {},
  error: null
};

export const actions = {
  changeFromCurrency: 'changeFromCurrency',
  changeToCurrency: 'changeToCurrency',
  changeFromAmount: 'changeFromAmount',
  changeToAmount: 'changeToAmount',
  updateUserAccounts: 'updateUserAccounts',
  updateRates: 'updateRates'
};

export const exchangeWidgetStore = (store: Store<IStore>) => {
  // Initial state
  store.on('@init', () => ({
    exchangeWidget: initialState
  }));

  store.on(actions.changeFromCurrency, (store, { currency, dispatch }) => {
    const { widgetId } = store.exchangeWidget;

    const currencyChanged = store.exchangeWidget.fromCurrency !== currency;

    if (currencyChanged) {
      refreshRatesService({
        id: widgetId,
        currency,
        updateRates: ratesTable => {
          dispatch(actions.updateRates, { ratesTable });
        }
      });

      return calculateNextWidgetState({
        ...store.exchangeWidget,
        fromCurrency: currency,
        ratesTable: {}
      });
    }

    return {
      exchangeWidget: store.exchangeWidget
    };
  });

  store.on(actions.changeToCurrency, (store, { currency }) => {
    return calculateNextWidgetState({
      ...store.exchangeWidget,
      toCurrency: currency
    });
  });

  store.on(actions.changeFromAmount, (store, { amount }) => {
    return calculateNextWidgetState({
      ...store.exchangeWidget,
      fromAmount: amount
    });
  });

  store.on(actions.changeToAmount, (store, { amount }) => {
    return calculateNextWidgetState({
      ...store.exchangeWidget,
      toAmount: amount
    });
  });

  store.on(actions.updateUserAccounts, (store, { userAccountsTable }) => {
    return calculateNextWidgetState({
      ...store.exchangeWidget,
      userAccountsTable
    });
  });

  store.on(actions.updateRates, (store, { ratesTable }) => {
    return calculateNextWidgetState({
      ...store.exchangeWidget,
      ratesTable
    });
  });
};
