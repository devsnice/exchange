import React from 'react';
import { shallow, ShallowWrapper, configure } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

configure({ adapter: new Adapter() });

import { ExchangeWidget } from './ExchangeWidget';
import { CURRENCY } from '../../typings/currencies';

jest.mock('react-styled-select', () => {});
jest.mock('storeon/react', () => () => ({
  dispatch: jest.fn(),
  exchangeWidget: {}
}));

const mockAccounts = [
  { amount: 1000.5, currency: CURRENCY.EUR },
  { amount: 9000.99, currency: CURRENCY.USD },
  { amount: 44444.99, currency: CURRENCY.GBP }
];

describe('ExchangeWidget', () => {
  test('should have a correct layout', () => {
    let wrapper: ShallowWrapper = shallow(
      <ExchangeWidget accounts={mockAccounts} />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
