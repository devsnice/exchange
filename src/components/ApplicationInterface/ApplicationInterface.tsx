import React from 'react';
import styled from 'styled-components';
import useStoreon from 'storeon/react';

import { ExchangeWidget } from '../ExchangeWidget/ExchangeWidget';
import { userActions } from '../../storeon/user/user';
import { IDataForExchange } from '../../typings/exchangeInfo';

const StyledApplicationInterface = styled.div`
  margin: 100px;
`;

export const ApplicationInterface: React.FC = () => {
  const { dispatch, user } = useStoreon('user');

  return (
    <StyledApplicationInterface>
      <ExchangeWidget
        accounts={user.accounts}
        exchangeMoney={(formData: IDataForExchange) => {
          dispatch(userActions.exchangeMoney, {
            formData
          });
        }}
      />
    </StyledApplicationInterface>
  );
};
