import React from 'react';
import styled from 'styled-components';
// @ts-ignore
import Select from 'react-styled-select';

import { Label } from '../Label/Label';
import { Nullable } from '../../../typings/utils';

const SelectContainer = styled.div`
  width: 100%;
`;

export type SelectOption = { label: string; value: string };

type Props = {
  label: string;
  value: Nullable<string>;
  options: SelectOption[];
  onChange: (value: string) => void;
  valueRenderer?: (option: SelectOption) => React.ReactElement;
};

export const CustomSelect: React.FC<Props> = ({
  label,
  value,
  options,
  valueRenderer,
  onChange
}) => {
  return (
    <SelectContainer>
      <Label>{label}</Label>

      <Select
        className="widget-theme"
        options={options}
        value={value}
        valueRenderer={valueRenderer}
        onChange={onChange}
      />
    </SelectContainer>
  );
};
