import { CURRENCY } from './currencies';

export type ExchangeInfo = number;

export type IDataForExchange = {
  fromCurrency: CURRENCY;
  toCurrency: CURRENCY;
  amount: number;
  rate: number;
};
