# Currency exchange widget

There's solution of test task by Borisov Semyon

## Сomments

There's real exchange rates only for exchangings from EUR-account. For USD and GPB I use fakes, which depends on EUR-rates.

## Tech stack

Typescript/React/Storeon/Styled-components/

**Stage managment system**

Why?

First of all, for separation a view of widget from it's logic. Business logic makes more sense, when you can read it in one place.
It's more convenient for developing and refactoring in the future, and it's much more readable.

I think the best solution for such task is reactive programming. There're many diffrent sources of data,
which produce changes inside widget. But I didn't choose Mobx or kind of this, cause it's too complex inside itself.

I really like, when I can see what my program does step-by-step.

I used *storeon* to provide a deterministic changes inside widget, 
redux is convenient for it too, but there're more boilerplate code. 
Also it was pretty interesting to use a new tool in this task.

## How to run

```
yarn i
yarn start
```

