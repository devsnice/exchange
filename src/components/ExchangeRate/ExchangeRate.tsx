import React from 'react';
import styled from 'styled-components';
import { CURRENCY_SIGN, CURRENCY } from '../../typings/currencies';
import { Nullable } from '../../typings/utils';
import { roundMoneyToFourDigits } from '../../utils/utils';

const StyledContainer = styled.span`
  font-size: 16px;
  color: rgba(50, 162, 29, 0.87);
`;

type Props = {
  currencyFrom: Nullable<CURRENCY>;
  currencyTo: Nullable<CURRENCY>;
  rate: Nullable<number>;
};

export const ExchangeRate: React.FC<Props> = ({
  currencyFrom,
  currencyTo,
  rate
}) => {
  if (!(currencyFrom && currencyTo && rate)) return null;

  return (
    <StyledContainer>
      1{CURRENCY_SIGN[currencyFrom]} = {roundMoneyToFourDigits(rate)}
      {CURRENCY_SIGN[currencyTo]}
    </StyledContainer>
  );
};
