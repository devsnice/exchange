import { CURRENCY } from './currencies';

export type MoneyAccount = {
  amount: number;
  currency: CURRENCY;
};
