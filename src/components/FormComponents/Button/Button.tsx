import styled from 'styled-components';

export const Button = styled.button`
  border: 1px solid rgba(30, 174, 104, 0.5);
  border-radius: 4px;
  font-size: 16px;
  color: #1eae68;
  height: 45px;
  padding: 0 35px;
  cursor: pointer;
  outline: none;

  &:hover {
    background: rgba(30, 174, 104, 0.9);
    color: #ffffff;
  }

  &:disabled {
    background: rgba(226, 226, 226, 0.5);
    border: 0;
    color: #bdbdbd;
    cursor: default;
  }
`;
