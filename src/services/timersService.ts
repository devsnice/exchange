class TimersService {
  private timers: Record<string, number> = {};

  public saveTimer = (name: string, timer: number) => {
    this.timers[name] = timer;
  };

  public getTimer = (name: string): number | undefined => {
    return this.timers[name];
  };
}

const TimersServiceInstance = new TimersService();

export { TimersServiceInstance as TimersService };
